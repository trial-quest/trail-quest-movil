package com.example.trailquest.viewmodel

import com.example.trailquest.model.Resena
import com.example.trailquest.model.Tesoro

interface ResenaClickListener {
    fun onClick(resena: Resena)
}