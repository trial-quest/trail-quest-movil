package com.example.trailquest.viewmodel

import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.trailquest.model.Jugador
import com.example.trailquest.model.Resena
import com.example.trailquest.model.Tesoro
import com.example.trailquest.model.TesoroUsuario
import com.example.trailquest.view.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URI
import java.util.concurrent.CopyOnWriteArrayList

class TesoroViewModel: ViewModel() {
    lateinit var repository: Repository
    var data = MutableLiveData<List<Tesoro>>()
    val tesoro= MutableLiveData<Tesoro>()
    var tesoroUsuario= MutableLiveData<List<TesoroUsuario>>()
    var resenas= MutableLiveData<List<Resena>>()
    var currentTesoro: Tesoro? = null
    var currentUser= MutableLiveData<Jugador>()
    var name=""
    var users= MutableLiveData<List<Jugador>>()
    var dataEncontrados = MutableLiveData<List<Tesoro>>()
    var tesoros = mutableListOf<Tesoro>()
    var descubiertos = mutableListOf<Tesoro>()
    var fromMap = false
    var camara = false
    var loginClean = false


    var fotohecha = true
    var image : Uri? = null

    fun fetchData(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getTesoro("/tesoros")
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    data.postValue(response.body())
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
    fun fetchTesoroByName(Name: String){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getTesoroByName("/tesoros/$Name")
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    data.postValue(response.body()!!)
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
    fun tesorosEncontrados(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getTesorosEncontrados("/tesoros/${currentUser.value?.usu_id.toString()}/encontrados")
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    println(response.body())
                    if (response.body().isNullOrEmpty()) tesoroUsuario.postValue(mutableListOf())
                    else {
                        tesoroUsuario.postValue(response.body()!!)
                    }
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
    fun setSelectedUser(tesoro: Tesoro) {
        currentTesoro = tesoro
    }
    fun resenasTesoro(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getResenasTesoro("/tesoros/${currentTesoro?.id}/resena")
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    resenas.postValue(response.body()!!)
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
    fun getUsers(){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getUsers("/jugador")
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    users.postValue(response.body()!!)
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }
    fun postTesoroDescubierto(){
        CoroutineScope(Dispatchers.IO).launch {
         repository.postTesoroDescubierto(currentUser.value!!.usu_id.toString(), currentTesoro!!.id.toString())
        }
    }
    fun postResena(resena: Resena, image: Uri?){
        CoroutineScope(Dispatchers.IO).launch {
            repository.postResena(currentUser.value?.usu_id.toString(), resena.tes_id.toString(), resena.user_name, "", resena.comentario, resena.puntuacion, image)
        }
    }
    fun setValueData(encontrados: MutableList<Tesoro>){
        dataEncontrados.postValue(encontrados)
    }

    fun deleteTesoro (tesId:Int){
        CoroutineScope(Dispatchers.IO).launch {
        repository.deleteTesoros("tesoros/${currentUser.value?.usu_id}/encontrados/$tesId")
        }

    }


}