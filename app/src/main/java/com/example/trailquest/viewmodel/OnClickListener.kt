package com.example.trailquest.viewmodel

import com.example.trailquest.model.Tesoro

interface OnClickListener {
    fun onClick(tesoro: Tesoro)
}