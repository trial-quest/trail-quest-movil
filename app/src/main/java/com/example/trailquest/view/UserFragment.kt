package com.example.trailquest.view

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.graphics.drawable.ShapeDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.trailquest.R
import com.example.trailquest.databinding.FragmentUserBinding
import com.example.trailquest.viewmodel.TesoroViewModel

class UserFragment : Fragment() {
    lateinit var binding: FragmentUserBinding
    private val viewModel: TesoroViewModel by activityViewModels()
    val porcentage=Double
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserBinding.inflate(layoutInflater)
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.nombreUsuario.text= viewModel.currentUser.value?.usu_username

        val imageView: ImageView = view.findViewById(R.id.foto_perfil)
        val layerDrawable: LayerDrawable = imageView.background as LayerDrawable
        val borderDrawable: GradientDrawable = layerDrawable.getDrawable(0) as GradientDrawable

        // Realiza el cambio de color del borde
        val colorAnimator = ObjectAnimator.ofArgb(
            borderDrawable, "color", Color.CYAN, Color.GREEN) // Color inicial y final del borde

        colorAnimator.duration = 1000 // Duración de la animación en milisegundos
        colorAnimator.repeatCount = ValueAnimator.INFINITE // Repetir la animación infinitamente
        colorAnimator.repeatMode = ValueAnimator.REVERSE // Invertir la animación al repetirse

        colorAnimator.start()

        viewModel.tesorosEncontrados()
        viewModel.fetchData()
        var tesorosEncontrados=0.0
        if (viewModel.tesoroUsuario.value?.size?.toDouble() != null){
             tesorosEncontrados = viewModel.tesoroUsuario.value?.size?.toDouble()!!
        }
        val tesorosTotales = viewModel.data.value?.size?.toDouble()
        val porcentage= tesorosTotales?.let { viewModel.descubiertos.size.div(it) }?.times(100)
        binding.nivelProgress.max= tesorosTotales!!.toInt()
        binding.nivelProgress.progress = viewModel.descubiertos.size
        when(porcentage!!){
            in 0.0..30.00 ->{binding.nivelValue.text= "NOVATO"}
            in 30.1..50.00 ->{binding.nivelValue.text= "AFICIONADO"}
            in 50.1..70.00 ->{binding.nivelValue.text= "INTERMEDIO"}
            in 70.1..90.00 ->{binding.nivelValue.text= "AVANZADO"}
            in 90.1..99.99 ->{binding.nivelValue.text= "PROFESIONAL"}
            100.00->{binding.nivelValue.text= "LEYENDA"}
        }
        binding.encontradosValue.text= "${viewModel.descubiertos.size}/${tesorosTotales.toInt() }"


        binding.button.setOnClickListener {
            viewModel.loginClean = true
            findNavController().navigate(R.id.action_userFragment_to_loginFragment)
        }
    }
}