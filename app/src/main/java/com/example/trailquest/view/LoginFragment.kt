package com.example.trailquest.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.trailquest.R
import com.example.trailquest.databinding.FragmentLoginBinding
import com.example.trailquest.model.Jugador
import com.example.trailquest.viewmodel.TesoroViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginFragment : Fragment() {
    lateinit var binding: FragmentLoginBinding
    private val viewModel: TesoroViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)

        if (viewModel.loginClean){
            binding.usernameEdittext.text.clear()
            binding.passwordEdittext.text.clear()
            viewModel.loginClean = false
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.loginButton.setOnClickListener {
            if (binding.usernameEdittext.text.toString()!="" && binding.passwordEdittext.text.toString() != ""){
                viewModel.currentUser.value = Jugador(0,binding.usernameEdittext.text.toString(),binding.passwordEdittext.text.toString(),"perfil.png")
                viewModel.repository = Repository(binding.usernameEdittext.text.toString(),binding.passwordEdittext.text.toString())
                CoroutineScope(Dispatchers.IO).launch {
                    val repository = Repository(binding.usernameEdittext.text.toString(),binding.passwordEdittext.text.toString())
                    val response = repository.login(viewModel.currentUser.value!!)
                    withContext(Dispatchers.Main) {
                        if(response.isSuccessful){
                            Toast.makeText(context, "Bienvenido ${viewModel.currentUser.value!!.usu_username}", Toast.LENGTH_SHORT).show()
                            findNavController().navigate(R.id.action_loginFragment_to_mapFragment)
                        }
                        else{
                            Toast.makeText(context, "Error to login", Toast.LENGTH_SHORT).show()
                        }
                    }
                }

            }
            else{
                Toast.makeText(context, "Error to register", Toast.LENGTH_SHORT).show()
            }

        }
        binding.signup.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }


}