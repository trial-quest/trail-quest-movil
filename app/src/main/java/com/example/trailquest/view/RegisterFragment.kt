package com.example.trailquest.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.trailquest.R
import com.example.trailquest.databinding.FragmentRegisterBinding
import com.example.trailquest.model.Jugador
import com.example.trailquest.viewmodel.TesoroViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RegisterFragment : Fragment() {
    lateinit var binding: FragmentRegisterBinding
    private val viewModel: TesoroViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.registerButton.setOnClickListener {
            if (binding.username.text.toString()!= "" && binding.password.text.toString() != "" && binding.repeat.text.toString() != "" && binding.correo.text.toString() != "" && binding.password.text.toString()== binding.repeat.text.toString()){
                viewModel.currentUser.value = Jugador(0,binding.username.text.toString(),binding.password.text.toString(),"perfil.png")
                viewModel.repository = Repository(binding.username.text.toString(),binding.password.text.toString())
                CoroutineScope(Dispatchers.IO).launch {
                    val repository = Repository(binding.username.text.toString(),binding.password.text.toString())
                    val response = repository.register(viewModel.currentUser.value!!)
                    withContext(Dispatchers.Main) {
                        if(response.isSuccessful){
                            Toast.makeText(context, "Welcome ${viewModel.currentUser.value!!.usu_username}", Toast.LENGTH_SHORT).show()
                            findNavController().navigate(R.id.action_registerFragment_to_mapFragment)
                        }
                        else{
                            Toast.makeText(context, "Error to register", Toast.LENGTH_SHORT).show()
                        }
                    }
                }

            }
            else{
                Toast.makeText(context, "Error to register", Toast.LENGTH_SHORT).show()
            }

        }
        binding.atras.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
    }

}