package com.example.trailquest.view

import com.burgstaller.okhttp.digest.Credentials
import com.burgstaller.okhttp.digest.DigestAuthenticator
import com.example.trailquest.model.Jugador
import com.example.trailquest.model.Resena
import com.example.trailquest.model.Tesoro
import com.example.trailquest.model.TesoroUsuario
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Url

interface ApiInterface {
    @GET()
    suspend fun getTesoro(@Url url: String): Response<List<Tesoro>>
    @GET()
    suspend fun getTesorosEncontrados(@Url url: String): Response<List<TesoroUsuario>>
    @GET()
    suspend fun getTesoroByName(@Url url: String): Response<List<Tesoro>>
    @GET
    suspend fun getPhoto(@Url url: String): Response<ResponseBody>
    @GET
    suspend fun getUsers(@Url url: String): Response<List<Jugador>>
    @GET()
    suspend fun getResenasTesoro(@Url url: String): Response<List<Resena>>
    @POST("/tesoros/{user_id}/descubiertos/{tes_id}")
    suspend fun postTesoroDescubierto(@Path ("user_id") userId: String, @Path ("tes_id") tesID: String): Response<ResponseBody>
    @Multipart
    @POST("/tesoros/{user_id}/resena/{tes_id}")
    suspend fun postResenaTesoro(
        @Path ("user_id") userId: String,
        @Path ("tes_id") tesID: String,
        @Part resenaData: MultipartBody.Part,
        @Part image: MultipartBody.Part): Response<Resena>
    @POST("user/login")
    suspend fun login(@Body jugador: Jugador): Response<ResponseBody>
    @POST("user/register")
    suspend fun register(@Body jugador: Jugador): Response<ResponseBody>

    @DELETE ()
    suspend fun deleteTesoro (@Url url: String): Response<ResponseBody>

    companion object {
        val BASE_URL = "http://135.181.182.115:8081/"

//url alex pc = 172.23.7.123
        //url david pc = 172.23.7.116

        fun create(username: String, password: String): ApiInterface {
            val digestAuthenticator = DigestAuthenticator(Credentials(username, password))

            val client = OkHttpClient.Builder()
                .authenticator(digestAuthenticator)
                .build()

            val gson = GsonBuilder().setLenient().create()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }

}