package com.example.trailquest.view
import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import com.example.trailquest.R
import com.example.trailquest.databinding.FragmentMapBinding
import com.example.trailquest.model.Tesoro
import com.example.trailquest.viewmodel.TesoroViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingClient
import com.google.android.gms.location.GeofencingRequest
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
const val REQUEST_CODE_LOCATION = 100
var capas = ""
class MapFragment : Fragment(), OnMapReadyCallback {

    lateinit var map: GoogleMap
    private val viewModel: TesoroViewModel by activityViewModels()
    private lateinit var binding: FragmentMapBinding
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    var result = FloatArray(1)
    private val isLoading = MutableLiveData<Boolean>()
    val idsDescubiertos= mutableListOf<Int>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)
        binding = FragmentMapBinding.inflate(inflater, container, false)
        binding.username.text= "Buenas " + viewModel.currentUser.value?.usu_username
        createMap()
        binding.ayuda.setOnClickListener {
            val builder = AlertDialog.Builder(requireContext())
            val view = layoutInflater.inflate(R.layout.dialoghelp, null)

            builder.setView(view)

            val dialog = builder.create()
            dialog.show()
        }
        return binding.root
    }

    private fun createMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as     SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    @SuppressLint("PotentialBehaviorOverride")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getUsers()
        viewModel.users.observe(viewLifecycleOwner) {
            for (i in it) {
                if (i.usu_username == viewModel.currentUser.value!!.usu_username) {
                    viewModel.currentUser.postValue(i)
                }
            }
        }

        binding.ayuda.setOnClickListener {
            val builder = AlertDialog.Builder(requireContext())
            val view = layoutInflater.inflate(R.layout.dialoghelp, null)

            builder.setView(view)

            val dialog = builder.create()
            dialog.show()
        }
        binding.lista.setOnClickListener {
            findNavController().navigate(R.id.action_mapFragment_to_tesorosListaFragment)
        }
        binding.textviewcuadrado.setOnClickListener {
            findNavController().navigate(R.id.action_mapFragment_to_fragment_layers_map)
        }
        binding.capas.setOnClickListener {
            findNavController().navigate(R.id.action_mapFragment_to_fragment_layers_map)
        }

        isLoading.postValue(true)
        viewModel.fetchData()

        viewModel.data.observe(viewLifecycleOwner) {

            fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())


            val locationRequest = LocationRequest.create().apply {
                interval =
                    100000 // Intervalo de tiempo en milisegundos en el que deseas recibir actualizaciones de ubicación.
                fastestInterval =
                    50000 // El intervalo más rápido en el que puedes recibir actualizaciones de ubicación.
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY // Prioridad de alta precisión.
            }

            val locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    for (i in 0 until viewModel.data.value!!.size) {
                        val tesoroActual = it.get(i)
                        viewModel.currentTesoro = Tesoro(
                            tesoroActual.id,
                            tesoroActual.titulo,
                            tesoroActual.descripcion,
                            tesoroActual.valoracion,
                            tesoroActual.longitud,
                            tesoroActual.latitud,
                            tesoroActual.foto_tesoro
                        )

                        // Aquí recibirás las actualizaciones de ubicación
                        val location = locationResult.lastLocation
                        Location.distanceBetween(
                            it[i].latitud.toDouble(),
                            it[i].longitud.toDouble(),
                            location.latitude,
                            location.longitude,
                            result
                        )
                        if (result[0] <= 100.0) {
                            viewModel.currentTesoro = it[i]
                            break
                        }
                    }
                    if (result[0] <= 100.0) {
                        isLoading.postValue(false)
                    }
                }
            }

            val looper: Looper = Looper.getMainLooper()


            viewModel.tesorosEncontrados()
            viewModel.tesoroUsuario.observe(viewLifecycleOwner) { it1 ->
                for (i in it1) {
                    idsDescubiertos.add(i.tes_id)
                }


            // Solicitar actualizaciones de ubicación
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, looper)
        }

        isLoading.observe(viewLifecycleOwner) { isLoading ->
            if (!isLoading) {
                    if(viewModel.currentTesoro?.id !in idsDescubiertos){
                        viewModel.postTesoroDescubierto()
                        idsDescubiertos.add(viewModel.currentTesoro!!.id)
                        findNavController().navigate(R.id.action_mapFragment_to_fragment_treasure_detail)
                        viewModel.fromMap = true
                    }
                }

            }
            isLoading.postValue(true)
        }

    }

    @SuppressLint("PotentialBehaviorOverride")
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        createMarker()
        map.uiSettings.isZoomControlsEnabled = true
        map.uiSettings.isMapToolbarEnabled = true
        when(capas){
            "1" -> map.mapType = GoogleMap.MAP_TYPE_HYBRID
            "2" -> map.mapType = GoogleMap.MAP_TYPE_SATELLITE
            "3" -> map.mapType = GoogleMap.MAP_TYPE_TERRAIN
            "4" -> map.mapType = GoogleMap.MAP_TYPE_NORMAL
        }
        map.setInfoWindowAdapter(MarkerInfoWindowAdapter(requireContext()))
        enableLocation()
        val barcelonaLatLng = LatLng(41.3851, 2.1734)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(barcelonaLatLng, 12f))
    }

    fun createMarker(){
        viewModel.fetchData()
        viewModel.data.observe(viewLifecycleOwner) {
            for(i in it){
                val coordinates= LatLng(i.latitud.toDouble(),i.longitud.toDouble())
                var bitmap: Bitmap
                if(i.id !in idsDescubiertos){
                     bitmap= BitmapFactory.decodeResource(resources, R.drawable.chincheta)
                }
                else{
                    bitmap = BitmapFactory.decodeResource(resources, R.drawable.chincheta_verda)
                }
                val scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.width / 15, bitmap.height / 15, false)
                val myMarker = map.addMarker(MarkerOptions().position(coordinates).icon(BitmapDescriptorFactory.fromBitmap(scaledBitmap)).title(i.titulo))
                myMarker?.tag = i
                val circleCenter = LatLng(i.latitud.toDouble(), i.longitud.toDouble())
                val circleRadius = 100.0 // Radio en metros
                val circleOptions = CircleOptions()
                    .center(circleCenter)
                    .radius(circleRadius)
                    .strokeWidth(5f)
                    .strokeColor(Color.GRAY)
                    .fillColor(Color.argb(70, 128, 128, 128)) // Color de relleno
                map.addCircle(circleOptions)
            }
        }
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }
    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION
            )
        }
    }
    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }
    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }
    }
}