package com.example.trailquest.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.trailquest.R
import com.example.trailquest.databinding.FragmentEncontradosBinding
import com.example.trailquest.databinding.FragmentTesorosListaBinding
import com.example.trailquest.model.Adapter
import com.example.trailquest.model.AdapterEncontrados
import com.example.trailquest.model.Tesoro
import com.example.trailquest.viewmodel.OnClickListener
import com.example.trailquest.viewmodel.TesoroViewModel
import kotlinx.coroutines.runBlocking

class EncontradosFragment : Fragment(), OnClickListener {
    lateinit var binding: FragmentEncontradosBinding
    private val viewModel: TesoroViewModel by activityViewModels()
    private lateinit var userAdapter: AdapterEncontrados
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    var descubiertos = mutableListOf<Tesoro>()
    private val isLoading = MutableLiveData<Boolean>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEncontradosBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isLoading.postValue(true)
            viewModel.tesorosEncontrados()
            viewModel.tesoroUsuario.observe(viewLifecycleOwner) { it1 ->
                for (i in it1) {
                    println(i)
                    for (j in viewModel.data.value!!) {
                        if (i.tes_id == j.id) {
                            if (j !in descubiertos){
                                descubiertos.add(j)

                            } else{
                                viewModel.deleteTesoro(j.id)
                            }
                        }
                    }
                }
                viewModel.setValueData(descubiertos)
                viewModel.descubiertos = descubiertos
            }
        isLoading.postValue(false)

        isLoading.observe(viewLifecycleOwner) { isLoading ->
            if (!isLoading){
                viewModel.dataEncontrados.observe(viewLifecycleOwner) {
                    setUpRecyclerView(it)
                }
            }
        }

    }
        private fun setUpRecyclerView(listOfUsers: List<Tesoro>) {
            userAdapter = AdapterEncontrados(listOfUsers, this, TesoroViewModel())
            linearLayoutManager = LinearLayoutManager(context)
            binding.rvFavTreasureList.apply {
                setHasFixedSize(true) //Optimitza el rendiment de l’app
                layoutManager = linearLayoutManager
                adapter = userAdapter
            }
        }

        override fun onClick(user: Tesoro) {
            viewModel.setSelectedUser(user)
            findNavController().navigate(R.id.action_encontradosFragment2_to_fragment_treasure_detail)
        }


    }
