package com.example.trailquest.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.trailquest.R
import com.example.trailquest.databinding.FragmentLoginBinding
import com.example.trailquest.databinding.FragmentTesorosListaBinding
import com.example.trailquest.model.Adapter
import com.example.trailquest.model.Tesoro
import com.example.trailquest.viewmodel.OnClickListener
import com.example.trailquest.viewmodel.TesoroViewModel

class TesorosListaFragment : Fragment(), OnClickListener {

    lateinit var binding: FragmentTesorosListaBinding
    private val viewModel: TesoroViewModel by activityViewModels()
    private lateinit var userAdapter: Adapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTesorosListaBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.data.observe(viewLifecycleOwner){
            setUpRecyclerView(it)
        }
        binding.ayuda.setOnClickListener {
            val builder = AlertDialog.Builder(requireContext())
            val view = layoutInflater.inflate(R.layout.dialoghelp, null)

            builder.setView(view)

            val dialog = builder.create()
            dialog.show()
        }
        binding.mapa.setOnClickListener {
           requireActivity().onBackPressed()
        }
        binding.searchView.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    if (query != "") {
                        viewModel.name = query
                        viewModel.fetchTesoroByName(viewModel.name)
                        println(viewModel.tesoro.value)
                    }
                    else {
                        viewModel.fetchData()
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    if (newText.length >= 2) {
                        viewModel.name = newText
                        viewModel.fetchTesoroByName(viewModel.name)
                    }
                    else {
                        viewModel.fetchData()
                    }
                }
                return false
            }
        })
    }
    private fun setUpRecyclerView(listOfUsers: List<Tesoro>){
        userAdapter = Adapter(listOfUsers , this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
    }
    override fun onClick(user: Tesoro) {
        viewModel.setSelectedUser(user)
//        findNavController().navigate(R.id.action_tesorosListaFragment_to_fragment_treasure_detail)
    }


}