package com.example.trailquest.view

import android.net.Uri
import com.example.trailquest.model.Jugador
import com.example.trailquest.model.Resena
import com.google.gson.Gson
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody

import java.io.File

class Repository(username: String, password: String) {
    private val apiInterface = ApiInterface.create(username,password)
    suspend fun getTesoroByName(url: String)= apiInterface.getTesoroByName(url)
    suspend fun getTesoro(url: String) = apiInterface.getTesoro(url)
    suspend fun getTesorosEncontrados(url: String)= apiInterface.getTesorosEncontrados(url)
    suspend fun register(jugador: Jugador) = apiInterface.register(jugador)
    suspend fun getUsers(url: String)= apiInterface.getUsers(url)
    suspend fun login(jugador: Jugador) = apiInterface.login(jugador)
    suspend fun getResenasTesoro(url: String)= apiInterface.getResenasTesoro(url)
    suspend fun getImage(url: String)= apiInterface.getPhoto(url)
    suspend fun postTesoroDescubierto(userId: String, tesId: String)= apiInterface.postTesoroDescubierto(userId,tesId)

    suspend fun deleteTesoros(url: String) = apiInterface.deleteTesoro(url)

    suspend fun postResena(
        user_id: String,
        tes_id: String,
        user_name: String,
        foto: String,
        comentario: String,
        puntuacion: String,
        file: Uri?
    ) {
        val request = Resena(0,comentario,puntuacion,foto,tes_id.toInt(), user_id.toInt(), user_name)
        val file= File(file?.path )
        val gson = Gson()
        return try {
            val response = apiInterface.postResenaTesoro(
                user_id,
                tes_id,
                resenaData = MultipartBody.Part
                    .createFormData(
                        "resena_data",
                        gson.toJson(request)
                    ),
                image = MultipartBody.Part
                    .createFormData(
                       "image",
                        file.name,
                        file.asRequestBody()
                    )
            )

        }
        catch (e: Exception){
            println(e.message)
        }
    }

}
