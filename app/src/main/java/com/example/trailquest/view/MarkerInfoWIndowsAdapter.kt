package com.example.trailquest.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.RatingBar
import android.widget.TextView
import com.example.trailquest.R
import com.example.trailquest.model.Tesoro
import com.google.android.gms.location.places.Place
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class MarkerInfoWindowAdapter(private val context: Context) : GoogleMap.InfoWindowAdapter {


    override fun getInfoContents(p0: Marker): View? {
        // 1. Get tag
        val tesoro = p0?.tag as? Tesoro ?: return null

        // 2. Inflate view and set title, address, and rating
        val view = LayoutInflater.from(context).inflate(
            R.layout.marker_info_contents, null
        )
        view.findViewById<TextView>(
            R.id.text_view_title
        ).text = tesoro.titulo
        view.findViewById<RatingBar>(
            R.id.text_view_rating
        ).rating = 3f

        return view
    }

    override fun getInfoWindow(p0: Marker): View? {
        return null
    }
}