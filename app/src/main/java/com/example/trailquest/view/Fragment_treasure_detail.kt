package com.example.trailquest.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.trailquest.R
import com.example.trailquest.databinding.FragmentTreasureDetailBinding
import com.example.trailquest.model.AdapterResenas
import com.example.trailquest.model.Resena
import com.example.trailquest.viewmodel.ResenaClickListener
import com.example.trailquest.viewmodel.TesoroViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class Fragment_treasure_detail : Fragment(), ResenaClickListener {
    lateinit var binding: FragmentTreasureDetailBinding
    private val viewModel: TesoroViewModel by activityViewModels()
    val repository= Repository("admin", "password")
    private lateinit var userAdapter: AdapterResenas
    var valoracion =0.0F
    lateinit var linearLayoutManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTreasureDetailBinding.inflate(layoutInflater)
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)
        viewModel.resenasTesoro()
        var num= 0
        viewModel.resenas.observe(viewLifecycleOwner){
            setUpRecyclerView(it)
            for (i in it){
                if (i.puntuacion != ""){
                    num++
                    valoracion += i.puntuacion.toFloat()
                }
            }

            binding.ratingBar.rating = valoracion/num

        }
        binding.nameValue.text = viewModel.currentTesoro?.titulo
        binding.descripcion.text= viewModel.currentTesoro?.descripcion
        binding.resenas.setOnClickListener {
            binding.recyclerViewResenas.visibility= View.VISIBLE
            binding.descripcion.visibility= View.INVISIBLE
            binding.fondoresena.visibility = View.VISIBLE
            binding.fondodescripcion.visibility = View.INVISIBLE
            binding.addresena.visibility = View.VISIBLE
        }
        binding.resenasimage.setOnClickListener {
            binding.recyclerViewResenas.visibility= View.VISIBLE
            binding.descripcion.visibility= View.INVISIBLE
            binding.fondoresena.visibility = View.VISIBLE
            binding.fondodescripcion.visibility = View.INVISIBLE
            binding.addresena.visibility = View.VISIBLE
        }
        binding.titledescription.setOnClickListener {
            binding.recyclerViewResenas.visibility= View.INVISIBLE
            binding.descripcion.visibility= View.VISIBLE
            binding.fondoresena.visibility = View.INVISIBLE
            binding.fondodescripcion.visibility = View.VISIBLE
            binding.addresena.visibility = View.INVISIBLE
        }
        binding.document.setOnClickListener {
            binding.recyclerViewResenas.visibility= View.INVISIBLE
            binding.descripcion.visibility= View.VISIBLE
            binding.fondoresena.visibility = View.INVISIBLE
            binding.fondodescripcion.visibility = View.VISIBLE
            binding.addresena.visibility = View.INVISIBLE
        }
        binding.addresena.setOnClickListener {
            findNavController().navigate(R.id.action_fragment_treasure_detail_to_resenaFragment)
        }

        binding.atras.setOnClickListener {
            if (viewModel.fromMap){
                findNavController().navigate(R.id.action_fragment_treasure_detail_to_mapFragment)
                viewModel.fromMap = false
            }
            else{
                requireActivity().onBackPressed()
            }
        }

        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getImage("/tesoros/imagenes/${viewModel.currentTesoro?.foto_tesoro}")
            withContext(Dispatchers.Main){
                if(response.isSuccessful && response.body() != null){
                    val foto = response.body()!!.bytes()
                    context?.let {
                        Glide.with(it)
                            .load(foto)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
                            .circleCrop()
                            .into(binding.fotodescripcion)
                    }
                }
            }
        }
        return binding.root
    }
    private fun setUpRecyclerView(listOfResenas: List<Resena>){
        userAdapter = AdapterResenas(listOfResenas, this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerViewResenas.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
    }

    override fun onClick(resena: Resena) {
    }

}