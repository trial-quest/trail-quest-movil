package com.example.trailquest.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.trailquest.R
import com.example.trailquest.databinding.FragmentLayersMapBinding
import com.example.trailquest.databinding.FragmentLoginBinding

class Fragment_layers_map : Fragment() {
    lateinit var binding: FragmentLayersMapBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLayersMapBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.loginButton.setOnClickListener {
            findNavController().navigate(R.id.action_fragment_layers_map_to_mapFragment)
        }
        binding.fotohibrido.setOnClickListener {
            capas = "1"
            binding.hibridocheck.visibility = View.VISIBLE
            binding.satelitecheck.visibility = View.INVISIBLE
            binding.terrenocheck.visibility = View.INVISIBLE
            binding.normalcheck.visibility = View.INVISIBLE
        }
        binding.fotosatelite.setOnClickListener {
            capas = "2"
            binding.hibridocheck.visibility = View.INVISIBLE
            binding.satelitecheck.visibility = View.VISIBLE
            binding.terrenocheck.visibility = View.INVISIBLE
            binding.normalcheck.visibility = View.INVISIBLE
        }
        binding.fototerreno.setOnClickListener {
            capas = "3"
            binding.hibridocheck.visibility = View.INVISIBLE
            binding.satelitecheck.visibility = View.INVISIBLE
            binding.terrenocheck.visibility = View.VISIBLE
            binding.normalcheck.visibility = View.INVISIBLE
        }
        binding.fotonormal.setOnClickListener {
            capas = "4"
            binding.hibridocheck.visibility = View.INVISIBLE
            binding.satelitecheck.visibility = View.INVISIBLE
            binding.terrenocheck.visibility = View.INVISIBLE
            binding.normalcheck.visibility = View.VISIBLE
        }
    }
}