package com.example.trailquest.view

import android.Manifest
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.trailquest.R
import com.example.trailquest.databinding.FragmentResenaBinding
import com.example.trailquest.databinding.FragmentTreasureDetailBinding
import com.example.trailquest.model.AdapterResenas
import com.example.trailquest.model.Resena
import com.example.trailquest.viewmodel.TesoroViewModel

class ResenaFragment : Fragment() {
    lateinit var binding: FragmentResenaBinding
    private val viewModel: TesoroViewModel by activityViewModels()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentResenaBinding.inflate(layoutInflater)
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)
        if (viewModel.fotohecha){
            binding.fotocamara.setImageURI(viewModel.image)
        }
        binding.enviar.setOnClickListener {
            if (binding.text.text.toString() != "" && viewModel.fotohecha){
                val resena= Resena(0,binding.text.text.toString(),binding.value.rating.toString(),"",viewModel.currentTesoro?.id!!,viewModel.currentUser.value!!.usu_id,viewModel.currentUser.value!!.usu_username )
                viewModel.postResena(resena, viewModel.image)
                viewModel.resenasTesoro()
                findNavController().navigate(R.id.action_resenaFragment_to_fragment_treasure_detail)
            }
            else{
                Toast.makeText(context, "Error al enviar reseña", Toast.LENGTH_LONG).show()
            }
        }
        binding.cancelar.setOnClickListener {
            findNavController().navigate(R.id.action_resenaFragment_to_fragment_treasure_detail)
        }
        binding.camara.setOnClickListener {
            findNavController().navigate(R.id.action_resenaFragment_to_camaraFragment)
            viewModel.camara = true
        }
        return binding.root
    }

}