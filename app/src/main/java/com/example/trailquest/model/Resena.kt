package com.example.trailquest.model

import android.net.Uri

data class Resena(
    val res_id: Int,
    val comentario: String,
    val puntuacion: String,
    val foto: String,
    val tes_id: Int,
    val usu_id: Int,
    val user_name: String,
) {
    // Métodos y lógica adicional para la clase Resena
}
