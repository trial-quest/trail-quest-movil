package com.example.trailquest.model

data class Tesoro(
    val id: Int,
    val titulo: String,
    val descripcion: String,
    val valoracion: String,
    val longitud: String,
    val latitud: String,
    val foto_tesoro : String,

)
