package com.example.trailquest.model

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.trailquest.R
import com.example.trailquest.databinding.UserItemBinding
import com.example.trailquest.view.ApiInterface
import com.example.trailquest.view.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class Adapter (private var tesoros: List<Tesoro>, private val listener: com.example.trailquest.viewmodel.OnClickListener):RecyclerView.Adapter<Adapter.ViewHolder>() {
    private lateinit var context: Context
    lateinit var repository: Repository

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = UserItemBinding.bind(view)
        fun setListener(tesoro: Tesoro){
            binding.root.setOnClickListener{
                listener.onClick(tesoro)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        repository= Repository("admin", "password")
        val view = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return tesoros.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tesoro = tesoros[position]

        with(holder){
            setListener(tesoro)
            binding.textView.text = tesoro.titulo
            //binding.ratingBar. = tesoro.valoracion
            CoroutineScope(Dispatchers.IO).launch {
                val response = repository.getImage("/tesoros/imagenes/${tesoro.foto_tesoro}")
                withContext(Dispatchers.Main){
                    if(response.isSuccessful && response.body() != null){
                        val foto = response.body()!!.bytes()
                        Glide.with(context)
                            .load(foto)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
//                            .circleCrop()
                            .into(binding.imageView)
                    }
                }
            }

        }
    }

}