package com.example.trailquest.model

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.trailquest.R
import com.example.trailquest.databinding.UserItemBinding
import com.example.trailquest.view.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AdapterResenas (private var resenas: List<Resena>, private val listener: com.example.trailquest.viewmodel.ResenaClickListener):
    RecyclerView.Adapter<AdapterResenas.ViewHolder>() {
    private lateinit var context: Context
    lateinit var repository: Repository

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = UserItemBinding.bind(view)

        fun setListener(resena: Resena){
            binding.root.setOnClickListener {
                listener.onClick(resena)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        repository= Repository("admin", "password")
        val view = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return resenas.size
    }
    fun setMarkerList(markerList: MutableList<Resena>){
        this.resenas = markerList
        notifyDataSetChanged()
    }
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val resena = resenas[position]
        with(holder){
            setListener(resena)
            binding.textView.text = resena.comentario
            binding.ratingBar.setRating(resena.puntuacion.toFloat())
            CoroutineScope(Dispatchers.IO).launch {
                val response = repository.getImage("/tesoros/resenas/${resena.foto}")
                withContext(Dispatchers.Main){
                    if(response.isSuccessful && response.body() != null){
                        val foto = response.body()!!.bytes()
                        Glide.with(context)
                            .load(foto)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
                            .circleCrop()
                            .into(binding.imageView)
                    }
                }
            }
        }
    }

}