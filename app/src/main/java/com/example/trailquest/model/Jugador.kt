package com.example.trailquest.model

data class Jugador(
    val usu_id: Int,
    val usu_username: String,
    val usu_password: String,
    var usu_foto: String
)