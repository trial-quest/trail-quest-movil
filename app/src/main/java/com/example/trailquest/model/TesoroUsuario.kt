package com.example.trailquest.model

import java.time.LocalDate
import java.util.Date

data class TesoroUsuario(
    val tu_id: Int,
    val tu_descubierto: Boolean,
    val date: LocalDate,
    val tes_id: Int,
    val usu_id: Int
)