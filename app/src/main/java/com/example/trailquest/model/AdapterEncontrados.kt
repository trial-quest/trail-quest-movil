package com.example.trailquest.model

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.trailquest.R
import com.example.trailquest.databinding.UserItemBinding
import com.example.trailquest.view.Repository
import com.example.trailquest.viewmodel.TesoroViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.properties.Delegates

class AdapterEncontrados (private var tesoros: List<Tesoro>, private val listener: com.example.trailquest.viewmodel.OnClickListener, private val viewModel : TesoroViewModel):
    RecyclerView.Adapter<AdapterEncontrados.ViewHolder>() {
    private lateinit var context: Context
    lateinit var repository: Repository
    var valoracion = 0.0

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = UserItemBinding.bind(view)
        fun setListener(tesoro: Tesoro){
            binding.root.setOnClickListener{
                listener.onClick(tesoro)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        viewModel.repository= Repository("admin", "password")
        val view = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return tesoros.size
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){
            val tesoro = tesoros[position]
            setListener(tesoro)
            binding.textView.text = tesoro.titulo
//            binding.ratingBar.rating = tesoro.valoracion.toFloat()
            for (tresaure in viewModel.tesoros){
                if (tresaure.id == tesoro.id){
                    valoracion = tresaure.valoracion.toDouble()
                }
            }
            binding.ratingBar.rating = 2.5f

            CoroutineScope(Dispatchers.IO).launch {
                val response = viewModel.repository.getImage("/tesoros/imagenes/${tesoro.foto_tesoro}")
                withContext(Dispatchers.Main){
                    if(response.isSuccessful && response.body() != null){
                        val foto = response.body()!!.bytes()
                        Glide.with(context)
                            .load(foto)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
//                            .circleCrop()
                            .into(binding.imageView)
                    }
                }
            }

        }
    }

}