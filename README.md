# TRAIL QUEST APP

    Este repositorio contiene el código fuente de Trail Quest Movil, 
    una aplicación móvil desarrollada para revolucionar el mundo del turismo.
    
## Índice

1. Descripción del proyecto
2. Programas necesarios
3. Instalación
4. Tecnologias utilizadas
5. Cómo utilizarlo
6. Autores


##  Descripción del proyecto :confused: <a name="1"></a>

    Trail quest, hacemos de un viaje una experiencia inolvidable.


    Trail Quest consiste en la búsqueda de lugares emblemáticos en
    la ciudad de Barcelona. El usuario, a través de su geolocalización
    irá descubriendo lugares siempre y cuando se encuentre en un rango
    de 100 metros respecto al lugar. Una vez descubierto, se mostrará
    información relevante del lugar y tendrá la opción de agregar una
    reseña. Además, tendrá una lista de lugares encontrados junto a una
    lista de lugares totales dispuestos a ser descubiertos.
    
    
    ¿CÓMO SURGIÓ LA IDEA ?
    
    
    Trail Quest está pensado para hacer de guía turístico y que el usuario
    pueda hacer su propia ruta e ir a los lugares que más le interesen.
    De esta forma, puede ir descubriendo monumentos emblemáticos de
    forma divertida y didáctica.


## Programas necesarios  :eyes:

- Android Studio
- Git

## Instalación :hammer:

- `Paso 1`: Ve a la pagina: https://developer.android.com/studio
- `Paso 2`: Descarga Android Studio en la version acorde a tu sistema operativo
- `Paso 3`: Instalar Git: https://git-scm.com/downloads
- `Paso 4`: $ git clone https://gitlab.com/trial-quest/trail-quest-movil.git
- `Paso 5`: Abrir el proyecto con Android Studio
- `Paso 6`: Una vez abierto, arriba en el centro verás un icono de play (:arrow_forward:)
- `Paso 7`: Presiona el botón y a disfrutar

## Tecnologias utilizadas :rocket:

- Kotlin
- Git

## Cómo utilizarlo :question:

    Es muy sencillo, una vez instalado ves en busca de tu monumento
    favorito. Una vez dentro del rango, se mostrará la información de
    este y un listado de reseñas que otros usuarios han dejado.
    Tendrás la opción de dejar tu propia reseña junto a una foto del
    lugar. En la parte de abajo en el botón del medio tienes el listado
    de encontrados. A la derecha, las estadísticas del usuario. Por ultimo,
    el botón de la izquierda para volver al mapa.



## Autores :wave:

    ALEJANDRO TORRES LEMOS
    DAVID MORENO FERNÁNDEZ
    JORDI ROBLES PARERA
    DIEGO ARTEAGA QUEVEDO